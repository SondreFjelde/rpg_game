﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign1_RPG_Classes.RPGExceptions
{
    public class InvalidWeaponException : Exception
    {
        public override string Message
        {
            get
            {
                return "Targeted weapon can't be equipped in this slot";
            }
        }
    }
}
