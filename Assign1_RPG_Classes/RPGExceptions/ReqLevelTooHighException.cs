﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign1_RPG_Classes.RPGExceptions
{
    public class ReqLevelTooHighException : Exception
    {
        public override string Message
        {
            get
            {
                return "Level too low to equip this gear";
            }
        }
    }
}
