﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assign1_RPG_Classes.Items;

namespace Assign1_RPG_Classes.CharacterClasses
{
    public class Mage : Hero
    {

        public Mage(string name)
        {
            Name = name;
            Strength = TotalStr = 1;
            Dexterity = TotalDex = 1;
            Intelligence = TotalInt = 8;
            PrimaryAttr = TotalInt;
            LegalWeaponTypes = new List<WeaponBase.WeaponType>() { WeaponBase.WeaponType.Wand, WeaponBase.WeaponType.Staff };
            LegalArmorTypes = new List<ArmorBase.ArmorType>() { ArmorBase.ArmorType.Cloth };

        }

        //public override double DealDamage(double Intelligence);

        /// <summary>
        /// LevelUp uses the Hero LevelUp method, with the class' attribute gains as params
        /// </summary>
        public override void LevelUp()
        {
            AddLevelUpStats(1, 1, 5);
        }
    }
}
