﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assign1_RPG_Classes.Items;

namespace Assign1_RPG_Classes.CharacterClasses
{
    public class Warrior : Hero
    {


        public Warrior(string name)
        {
            Name = name;   
            Strength = TotalStr = 5;
            Dexterity = TotalDex = 2;
            Intelligence = TotalInt = 1;
            PrimaryAttr = Strength;
            LegalArmorTypes = new List<ArmorBase.ArmorType>() { Armor.ArmorType.Mail, Armor.ArmorType.Plate};
            LegalWeaponTypes = new List<WeaponBase.WeaponType>() { Weapon.WeaponType.Axe, Weapon.WeaponType.Hammer, Weapon.WeaponType.Sword};
            
        }

        /// <summary>
        /// LevelUp uses the Hero LevelUp method, with the class' attribute gains as params
        /// </summary>
        public override void LevelUp()
        {
            AddLevelUpStats(3, 2, 1);
        }
        

    }
}
