﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assign1_RPG_Classes.Items;

namespace Assign1_RPG_Classes.CharacterClasses
{
    public class Ranger : Hero
    {

        public Ranger(string name)
        {
            Name = name;
            Strength = TotalStr = 1;
            Dexterity = TotalDex = 7;
            Intelligence = TotalInt = 1;
            PrimaryAttr = TotalDex;
            LegalWeaponTypes = new List<WeaponBase.WeaponType>() { WeaponBase.WeaponType.Bow };
            LegalArmorTypes = new List<ArmorBase.ArmorType>() { ArmorBase.ArmorType.Leather, ArmorBase.ArmorType.Mail };

        }

        /// <summary>
        /// LevelUp uses the Hero LevelUp method, with the class' attribute gains as params
        /// </summary>
        public override void LevelUp()
        {
            AddLevelUpStats(1, 5, 1);
        }
    }
}
