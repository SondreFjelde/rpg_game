﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Assign1_RPG_Classes.Items;
using Assign1_RPG_Classes.RPGExceptions;


namespace Assign1_RPG_Classes.CharacterClasses
{
    public abstract class Hero
    {
        public int level = 1;
        public int Level { get => level; set => level = value; } 
        public string ClassName { get; set; }
        public string Name { get; set; }
        public double Damage { get; set; }
        public double AttackSpd { get; set; }

        // base stats inherent to character

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public double PrimaryAttr { get; set; }

        public int TotalStr { get; set; }
        public int TotalDex { get; set; }
        public int TotalInt { get; set; }


        public List<Weapon.WeaponType> LegalWeaponTypes { get; set; }
        public List<Armor.ArmorType> LegalArmorTypes { get; set; }
        public Dictionary<Item.EquipSlots, Item> Equipment = new Dictionary<Item.EquipSlots, Item>();

        // Increases stats for level up
        public void AddLevelUpStats(int str, int dex, int intell)
        {
            Level++;
            Strength += str;
            Dexterity += dex;
            Intelligence += intell;
            //add try-catch
        }
        /// <summary>
        /// Method for levelling up a character. Each class has the stat additions for level up in the class-specific methods.
        /// </summary>
        public abstract void LevelUp();


        public double GetWeaponDPS()
        {
            double dps = Math.Floor(AttackSpd * Damage);
            return dps;
        }

        public double DealDamage(Weapon weapon)
        {
            double dps = weapon.WpnDPS;
            double heroDamage = dps * (1 + PrimaryAttr/100);
            return heroDamage;
        }


        /// <summary>
        /// Resets total attributes to base attributes, iterates through gear and adds stat bonuses from gear to total of its stat group
        /// </summary>
        public void EquipmentAddTotalAttributes()
        {
            TotalStr = Strength;
            TotalDex = Dexterity;
            TotalInt = Intelligence;

            foreach (Armor gear in Equipment.Values)
            {
                if (gear.EquipSlot == Armor.EquipSlots.Hand)
                {
                    continue;
                }
                int[] armStats = gear.GetArmorStats();
                TotalStr += armStats[0];
                TotalDex += armStats[1];
                TotalInt += armStats[2];
            }
        }

        /// <summary>
        /// Equips weapon to hand slot
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="equipSlot"></param>
        /// <returns></returns>
        public string EquipWeapon(Weapon weapon, Item.EquipSlots equipSlot = Item.EquipSlots.Hand)
        {
            if (Level < weapon.ReqLevel)
            {
                throw new ReqLevelTooHighException();
            }
            else if (weapon.EquipSlot != Item.EquipSlots.Hand|| !LegalWeaponTypes.Contains(weapon.WpnType))
            {
                throw new InvalidWeaponException();
            }
            Equipment.Add(Item.EquipSlots.Hand, weapon);
            return $"You have equipped {weapon.Name}";
        }

        /// <summary>
        /// Equips Armor to specified slot, if a requirement is not met. Then runs Attr add method.
        /// </summary>
        /// <param name="armor"></param>
        /// <param name="equipSlot"></param>
        /// <returns></returns>
        /// <exception cref="ReqLevelTooHighException"></exception>
        /// <exception cref="InvalidArmorException"></exception>
        public string EquipArmor(Armor armor, Item.EquipSlots equipSlot)
        {
            if (Level < armor.ReqLevel)
            {
                throw new ReqLevelTooHighException();
            }
            else if (equipSlot == Item.EquipSlots.Hand || !LegalArmorTypes.Contains(armor.ArmType))
            {
                throw new InvalidArmorException();
            }
            
            Equipment.Add(equipSlot, armor);
            EquipmentAddTotalAttributes();
            return $"You have equipped {armor.Name}";
        }

    }


}
