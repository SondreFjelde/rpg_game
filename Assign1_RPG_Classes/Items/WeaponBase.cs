﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign1_RPG_Classes.Items
{
    public class WeaponBase : Item
    {
        

        public double WpnSpd { get; set; }
        public double WpnDmg { get; set; }
        public double WpnDPS { get; set; }

        public enum WeaponType
        {
            Axe,
            Bow,
            Daggers,
            Hammer,
            Staff,
            Sword,
            Wand
        }


    }
}
