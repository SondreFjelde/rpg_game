﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign1_RPG_Classes.Items
{
    public abstract class Item
    {
        public string Name { get; set; }
        public int ReqLevel { get; set; }
        public EquipSlots EquipSlot { get; set; }

        public enum EquipSlots
        {
            Head,
            Body,
            Legs,
            Hand
        }

    }
}
