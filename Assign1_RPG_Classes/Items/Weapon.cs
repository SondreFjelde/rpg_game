﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assign1_RPG_Classes.Items.Item;


namespace Assign1_RPG_Classes.Items
{
    public class Weapon : WeaponBase
    {
        public WeaponType WpnType { get; set; }
        

        // adds name/title to be added to item type
        public Weapon(string name, WeaponType wpnType, int reqLevel, EquipSlots equipSlot)
        {
            Name = name;
            WpnType = wpnType;
            ReqLevel = reqLevel;
            EquipSlot = equipSlot;



            switch (WpnType)
            {
                case WeaponType.Axe:
                    WpnSpd = 1.0;
                    WpnDmg = 2.0;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;
                case WeaponType.Bow:
                    WpnSpd = 1.3;
                    WpnDmg = 1.7;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;
                case WeaponType.Daggers:
                    WpnSpd = 2.2;
                    WpnDmg = 0.8;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;
                case WeaponType.Hammer:
                    WpnSpd = 0.8;
                    WpnDmg = 2.2;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;
                case WeaponType.Staff:
                    WpnSpd = 1.6;
                    WpnDmg = 1.4;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;
                case WeaponType.Sword:
                    WpnSpd = 1.5;
                    WpnDmg = 1.5;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;
                case WeaponType.Wand:
                    WpnSpd = 2;
                    WpnDmg = 1;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;
                default:
                    WpnSpd = 1.5;
                    WpnDmg = 1.5;
                    WpnDPS = WpnSpd * WpnDmg;
                    break;

            }
        }

    }
}
