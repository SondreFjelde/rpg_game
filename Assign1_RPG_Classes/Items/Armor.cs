﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign1_RPG_Classes.Items
{
    public class Armor : ArmorBase
    {
        public ArmorType ArmType { get; set; }
        int StrBonus;
        int DexBonus;
        int IntBonus;

        public Armor(string name, int reqLevel, ArmorType armType)
        {
            ArmType = armType;
            ReqLevel = reqLevel;
            ArmType = armType;

            

            // set stat modifiers and name based on armortype
            switch (ArmType)
            {
                case ArmorType.Cloth:
                    Name = "Cloth Armor";
                    StrBonus = 0;
                    DexBonus = 0;
                    IntBonus = 4;
                    break;
                case ArmorType.Leather:
                    Name = "Leather Armor";
                    StrBonus = 1;
                    DexBonus = 3;
                    IntBonus = 0;
                    break;
                case ArmorType.Mail:
                    Name = "Mail Armor";
                    StrBonus = 2;
                    DexBonus = 2;
                    IntBonus = 0;
                    break;
                case ArmorType.Plate:
                    Name = "Plate Armor";
                    StrBonus = 4;
                    DexBonus = 0;
                    IntBonus = 0;
                    break;
                default:
                    Name = "Plain Clothes";
                    StrBonus = 1;
                    DexBonus = 1;
                    IntBonus = 1;
                    break;

            }    
        }
        // return stat bonuses in order: str, dex, int
        public int[] GetArmorStats()
        {
            int[] armStats = new int[3] { StrBonus, DexBonus, IntBonus };
            return armStats;
        }
    }
}
