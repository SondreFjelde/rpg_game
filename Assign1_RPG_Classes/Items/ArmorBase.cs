﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign1_RPG_Classes.Items
{
    public abstract class ArmorBase : Item
    {

        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
    }
}
