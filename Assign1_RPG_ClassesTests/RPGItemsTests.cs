﻿using System;
using Xunit;
using Assign1_RPG_Classes.CharacterClasses;
using Assign1_RPG_Classes.Items;
using Assign1_RPG_Classes.RPGExceptions;

namespace Assign1_RPG_ClassesTests
{
    public class RPGItemsTests
    {
        [Fact]
        public void WarriorEquipWeaponAxe_AxeReqLevel2_ShouldReturnInvalidWeaponException()
        {
            Warrior warrior = new("John");
            Weapon axe = new("axe", Weapon.WeaponType.Axe, 2, Weapon.EquipSlots.Hand);
            

            Assert.Throws<ReqLevelTooHighException>(() => warrior.EquipWeapon(axe));

        }

        [Fact]
        public void WarriorEquipArmorPlate_TypePlateLvl1ReqLvl1BodySlot_ShouldReturnString()
        {
            Warrior warrior = new("Kierkegaard");
            Armor plateArmor = new("Plate Armor", 1, Armor.ArmorType.Plate);

            string expected = "You have equipped Plate Armor";
            string actual = warrior.EquipArmor(plateArmor, Armor.EquipSlots.Body);

            Assert.Equal(expected, actual);

        }



        [Fact]
        public void MageDPS_Level1ReqLvl1EquipWand_ShouldReturnDouble()
        {
            Mage mage = new("Aromi");
            Weapon wand = new("Wand", WeaponBase.WeaponType.Wand, 1, Item.EquipSlots.Hand);
            mage.EquipWeapon(wand);

            double expected = 2;
        }

        [Fact]
        public void MageDealDamage_Level1ReqLevel1EquipWand_ShouldReturnDouble3Point3()
        {
            Mage mage = new("Aromi");
            Weapon wand = new("Wand", WeaponBase.WeaponType.Wand, 1, Item.EquipSlots.Hand);
            mage.EquipWeapon(wand);

            double expected = 3.3;
            double actual = mage.DealDamage(wand);

            Assert.Equal(expected, actual);
        }

    }
}
