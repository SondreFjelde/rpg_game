using System;
using Xunit;
using Assign1_RPG_Classes.CharacterClasses;
using Assign1_RPG_Classes.Items;

namespace Assign1_RPG_ClassesTests
{
    public class RPGClassesTests
    {
        /// <summary>
        /// Create hero warrior, and check if level is 1 at creation
        /// </summary>
        [Fact]
        public void CreateWarrior_NewWarriorLevel_LevelIsInt1()
        {

            // Arrange
            Warrior warrior = new("Dallas");
            int expected = 1;

            // Act
            int actual = warrior.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        //public void CreateMage_NewMage()
        //{

        //}

        /// <summary>
        /// Check if hero is level 2 at first level up
        /// </summary>
        [Fact]
        public void LevelUp_WarriorLevelUp_ShouldReturn2()
        {
            Warrior warrior = new("Florida");
            warrior.LevelUp();
            int expected = 2;

            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// chekcs if hero classes are the correct stats at creation & level up
        /// </summary>

        [Fact]
        public void CreateWarrior_CheckHeroStrength_ShouldReturn5()
        {
            Warrior warrior = new("Washington");
            int expected = 5;

            int actual = warrior.Strength;

            Assert.Equal(expected, actual);

            expected = 2;

            actual = warrior.Dexterity;

            Assert.Equal(expected, actual);

            expected = 1;

            actual = warrior.Intelligence;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateWarrior_CheckHeroDex_ShoudReturn2()
        {
            Warrior warrior = new("Mississippi");
            int expected = 2;

            int actual = warrior.Dexterity;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateWarrior_CheckHeroIntelligence_ShoudReturn1()
        {
            Warrior warrior = new("Utah");
            int expected = 1;

            int actual = warrior.Intelligence;

            Assert.Equal(expected, actual);
        }


        [Fact]
        public void AddLevelUpStats_WarriorAttrIsDefaultLevel1LevelUp_ShouldReturnInt()
        {
            Warrior warrior = new("Idaho");
            warrior.LevelUp();
            int expectedStr = 8;
            int expectedDex = 4;
            int expectedInt = 2;

            int actualStr = warrior.Strength;
            int actualDex = warrior.Dexterity;
            int actualInt = warrior.Intelligence;

            Assert.Equal(expectedStr, actualStr);
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
        }

        [Fact]
        public void CheckMageTotalStat_MageTotalAttrIsDefaultLevel1EquipArmorLvl1_ShouldReturnInt()
        {
            Mage mage = new("Theolin");
            Weapon staff = new("Staff", WeaponBase.WeaponType.Staff, 1, Item.EquipSlots.Hand);
            Armor armor = new("Cloth Armor", 1, ArmorBase.ArmorType.Cloth);
            mage.EquipWeapon(staff);
            mage.EquipArmor(armor, Item.EquipSlots.Body);

            double expected = 12;

            double actual = mage.PrimaryAttr;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddLevelUpStats_MageAttrIsDefaultLevel1LevelUp_ShouldReturnInt()
        {
            Mage mage = new("Idaho");
            mage.LevelUp();
            int expectedStr = 2;
            int expectedDex = 2;
            int expectedInt = 13;

            int actualStr = mage.Strength;
            int actualDex = mage.Dexterity;
            int actualInt = mage.Intelligence;

            Assert.Equal(expectedStr, actualStr);
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
        }

        [Fact]
        public void AddLevelUpStats_RangerAttrIsDefaultLevel1LevelUp_ShouldReturnInt()
        {
            Ranger ranger = new("Idaho");
            ranger.LevelUp();
            int expectedStr = 2;
            int expectedDex = 12;
            int expectedInt = 2;

            int actualStr = ranger.Strength;
            int actualDex = ranger.Dexterity;
            int actualInt = ranger.Intelligence;

            Assert.Equal(expectedStr, actualStr);
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
        }

        [Fact]
        public void AddLevelUpStats_RoguerAttrIsDefaultLevel1LevelUp_ShouldReturnInt()
        {
            Rogue rogue = new("Idaho");
            rogue.LevelUp();
            int expectedStr = 3;
            int expectedDex = 10;
            int expectedInt = 2;

            int actualStr = rogue.Strength;
            int actualDex = rogue.Dexterity;
            int actualInt = rogue.Intelligence;

            Assert.Equal(expectedStr, actualStr);
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
        }
    }
}
